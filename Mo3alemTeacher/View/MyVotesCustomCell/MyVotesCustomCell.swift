//
//  MyVotesCustomCell.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit
import Cosmos

class MyVotesCustomCell: UITableViewCell {

    
    @IBOutlet weak var cellViRate: CosmosView!
    @IBOutlet weak var cellLblName: UILabel!
    @IBOutlet weak var cellLblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
