//
//  IncomingOrderCustomCell.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/19/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class IncomingOrderCustomCell: UITableViewCell {

    @IBOutlet weak var cellStackContainer: UIStackView!
    @IBOutlet weak var cellLblStatus: UILabel!
    @IBOutlet weak var cellLblTitle: UILabel!
    @IBOutlet weak var cellBtnRefuse: UIButton!
    @IBOutlet weak var cellBtnAccept: UIButton!
    @IBOutlet weak var cellLblTime: UILabel!
    @IBOutlet weak var cellLblDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellLblDate.layer.cornerRadius = 5.0
        cellLblDate.layer.masksToBounds = true
        cellLblTime.layer.cornerRadius = 5.0
        cellLblTime.layer.masksToBounds = true
        
        cellLblTime.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1).cgColor
        cellLblTime.layer.borderWidth = 0.7
        cellLblDate.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1).cgColor
        cellLblDate.layer.borderWidth = 0.7
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
