//
//  FinancialRecordsCustomCell.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/19/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class FinancialRecordsCustomCell: UITableViewCell {

    
    @IBOutlet weak var cellLblTitle: UILabel!
    @IBOutlet weak var cellLblDate: UILabel!
    @IBOutlet weak var cellImgStatus: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
