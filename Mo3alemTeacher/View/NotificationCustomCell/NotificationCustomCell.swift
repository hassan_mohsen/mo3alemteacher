//
//  NotificationCustomCell.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class NotificationCustomCell: UITableViewCell {
    
    
    @IBOutlet weak var cellLblTitle: UILabel!
    @IBOutlet weak var cellImgStatus: UIImageView!
    @IBOutlet weak var cellLblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
