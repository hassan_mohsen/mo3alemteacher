//
//  BanksSliderCustomCell.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class BanksSliderCustomCell: UICollectionViewCell {

    @IBOutlet weak var cellImgHeader: UIImageView!
    @IBOutlet weak var cellLblAccountNumber: UILabel!
    @IBOutlet weak var cellLblOwnerName: UILabel!
    @IBOutlet weak var cellViContainer: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
