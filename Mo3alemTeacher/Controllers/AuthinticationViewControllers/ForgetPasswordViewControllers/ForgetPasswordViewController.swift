//
//  ForgetPasswordViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/30/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {

    var parentVC : UIViewController!
    
    @IBOutlet weak var txtEmail: HMTextField!
    
    var forgetViewModel = ForgetPasswordViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        forgetViewModel.successEvent = {
            let vc = self.getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "EnterCodeViewController") as! EnterCodeViewController
            vc.parentVC = self.parentVC!
            vc.userEmail = self.txtEmail.text!
            self.dismiss(animated: true, completion: nil)
            self.parentVC.present(vc, animated: true, completion: nil)
        }
        
        forgetViewModel.failureEvent = { (message) in
            self.showErrorWithMessage(message: message)
        }
        
        
        
        
    }

    @IBAction func btnSendCode(_ sender: Any) {
        
        if txtEmail.text == nil {
            showErrorWithMessage(message: "من فضلك ادخل البريد الالكتروني .")
            return
        }
        
        if !((txtEmail.text?.isValidEmail())!) {
            showErrorWithMessage(message: "من فضلك ادخل بريد الكتروني صحيح .")
            return
        }
        
        
        forgetViewModel.setEmail(email: txtEmail.text!)
        forgetViewModel.sendRequest()
        
        
    }
    
    @IBAction func dismissButAct(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
}
