//
//  NewPasswordViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/30/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class NewPasswordViewController: UIViewController {

    @IBOutlet weak var txtPassword: HMTextField!
    @IBOutlet weak var txtRePassword: HMTextField!
    var userEmail : String!
    
    var restPassword = ResetPasswordViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        restPassword.successEvent = {
            self.showAlertWithTitleAndMessageWith1Handler(title: "تم", message: "تم تغير كلمه المرور بنجاح", doneTitle: "تم", doneHandler: { (alert) in
                self.dismiss(animated: true, completion: nil)
            })
        }
        
        restPassword.failureEvent = { (message) in
            self.showErrorWithMessage(message: message)
        }
        
    }

    @IBAction func changPassAct(_ sender: Any) {
        
        if txtPassword.text == nil {
            showErrorWithMessage(message: "من فضلك ادخل كلمه المرور الجديده")
            return
        }
        if txtRePassword.text == nil  {
            showErrorWithMessage(message: "من فضلك ادخل تكرار كلمه المرور .")
            return
        }
        if txtPassword.text != txtRePassword.text {
            showErrorWithMessage(message: "كلمه المرور وتكرار كلمه المرور غير متطابقتين ")
            return
        }
        
        restPassword.setData(email: userEmail, password: txtPassword.text!)
        restPassword.sendRequest()
        
        
        
    }
    
}
