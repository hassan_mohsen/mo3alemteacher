//
//  MainRegViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/21/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class MainRegViewController: UIViewController {

    
    @IBOutlet weak var txtUserName: HMTextField!
    @IBOutlet weak var txtPhone: HMTextField!
    @IBOutlet weak var txtEmail: HMTextField!
    @IBOutlet weak var txtPassword: HMTextField!
    @IBOutlet weak var txtRePassword: HMTextField!
    
    
    @IBOutlet weak var maleGenderBut: UIButton!
    @IBOutlet weak var femaleGenderBut: UIButton!
    
    
    var genderType : GenderAndType!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        unselectGenderType(but: maleGenderBut)
        unselectGenderType(but: femaleGenderBut)
        
    }
    
    
    func selectedGenderType(but : UIButton , unselect : UIButton ,gender : GenderAndType) -> Void {
        
        but.layer.borderWidth = 0
        but.backgroundColor = #colorLiteral(red: 0, green: 0.7725490196, blue: 0.831372549, alpha: 1)
        but.setTitleColor(.white, for: .normal)
        
        
        genderType = gender
        
        
        
        unselectGenderType(but: unselect)
    }
    
    func unselectGenderType(but : UIButton) -> Void {
        
        but.backgroundColor = .white
        but.layer.borderWidth = 0.8
        but.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
        but.setTitleColor(.lightGray, for: .normal)
        
        but.layer.cornerRadius = 5.0
        but.layer.masksToBounds = true
        
    }
    
    
    

}
