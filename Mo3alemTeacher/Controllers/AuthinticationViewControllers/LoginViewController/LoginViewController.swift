//
//  LoginViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/21/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {

    
    @IBOutlet weak var txtEmail: HMTextField!
    @IBOutlet weak var txtPassword: HMTextField!
    
    private var loginModel = LoginViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginModel.successEvent = {
            let vc = HomeTabBarViewController()
            self.present(vc, animated: true, completion: nil)
        }
        
        loginModel.failureEvent = { [unowned self] message in
            self.showErrorWithMessage(message: message)
        }
        
    }

    @IBAction func btnLoginAct(_ sender: Any) {
        
        if txtEmail.text == nil {
            showErrorWithMessage(message: "من فضلك ادخل البريد الالكتروني .")
            return
        }
        
        if !(txtEmail.text?.isValidEmail())!{
            showErrorWithMessage(message: "من فضلك ادخل بريد الكتروني صحيح .")
            return
        }
        
        if txtPassword.text == nil {
            showErrorWithMessage(message: "من فضلك ادخل كلمه المرور.")
            return
        }
        
        loginModel.setModelValues(email: txtEmail.textNoNil(), pass: txtPassword.textNoNil())
        
        loginModel.makeMeLogin()
        
        
        
        
        
    }
    
    @IBAction func btnRegisterAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "RegTeacherViewController") as! RegTeacherViewController
        present(vc, animated: true, completion: nil)
     }
    
    @IBAction func btnForgetAct(_ sender: Any) {
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "ForgetPasswordViewController") as! ForgetPasswordViewController
        vc.parentVC = self
        present(vc, animated: true, completion: nil)
    }
}
// hassan@gmail.com
// 123123123
