//
//  MessagesViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/20/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController {

    
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var constTextHeight: NSLayoutConstraint!
    
    
    
    private var messageModel :MessagesModel!
    
    
    var messageType : MessageType! {
        didSet{
            switch messageType! {
                
            case .success(let msg , let hidden):
                messageModel = MessagesModel.init(msg: msg.rawValue, img: "SuccessImg", color: #colorLiteral(red: 0.07843137255, green: 0.7490196078, blue: 0.5137254902, alpha: 1), btnHidden: hidden)
            case .failur(let msg , let hidden):
                messageModel = MessagesModel.init(msg: msg.rawValue, img: "FailurImg", color: #colorLiteral(red: 1, green: 0.3411764706, blue: 0.368627451, alpha: 1), btnHidden: hidden)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //===========================================
        
        imgStatus.image = messageModel.image
        txtMessage.text = messageModel.message
        btnDone.backgroundColor = messageModel.btnColor
        btnResend.isHidden = messageModel.ReSendHidden
        
        //===========================================
        let contentSize = txtMessage.contentSize
        constTextHeight.constant = contentSize.height + 10
        //============================================
    }
    
    
    @IBAction func btnResendAct(_ sender: Any) {
    }
    
    @IBAction func btnDoneAct(_ sender: Any) {
    }
    
    

   
}
