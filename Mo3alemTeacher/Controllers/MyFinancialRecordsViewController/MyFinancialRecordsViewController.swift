//
//  MyFinancialRecordsViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/19/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class MyFinancialRecordsViewController: UIViewController {

    
    @IBOutlet weak var TblRecords: UITableView!
    @IBOutlet weak var SegmentTypes: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TblRecords.register(UINib.init(nibName: "FinancialRecordsCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
    }

    
    @IBAction func SegmentChangedAct(_ sender: Any) {
        
        
        
    }
    

}




extension MyFinancialRecordsViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FinancialRecordsCustomCell
        
        
        return cell
    }
}


extension MyFinancialRecordsViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
}


