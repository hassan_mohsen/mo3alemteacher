//
//  IncommingOrderDetailsViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/27/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class IncommingOrderDetailsViewController: UIViewController {

    
    @IBOutlet weak var viRingTime: UIView!
    @IBOutlet weak var tblOrders: UITableView!
    //===================
    var timer : SwiftCountDownTimer!
    
    let shapLayer = CAShapeLayer()
    //===================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //=============================
        tblOrders.register(UINib.init(nibName: "IncomingOrderCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        //=============================
        
    }

    

}

//MARK:- setup ring Timer
extension IncommingOrderDetailsViewController
{
    
    
    func setupTimerAndRingAnimation() -> Void {
        timer = SwiftCountDownTimer(interval: .fromSeconds(1), times: 200, handler: {[unowned self] (timer, lefttimer) in
            
        })
        timer.start()
        
        
        let basicanimation = CABasicAnimation(keyPath: "strokeEnd")
        basicanimation.toValue = 1
        basicanimation.duration = 200
        basicanimation.fillMode = kCAFillModeForwards
        basicanimation.isRemovedOnCompletion = false
        
        
        shapLayer.add(basicanimation, forKey: "urSoBasic")
    }
    func setupRingLayer(viTest : UIView) -> Void {
        
        //        let center = viTest.center
        let center = CGPoint(x: viTest.bounds.width / 2.0, y: viTest.bounds.height / 2.0)
        // creat track layer
        let trackLayer = CAShapeLayer()
        let circlePath = UIBezierPath(arcCenter: center, radius: 48, startAngle: -CGFloat.pi/2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circlePath.cgPath
        trackLayer.strokeColor = #colorLiteral(red: 0.5294117647, green: 0.831372549, blue: 0.7176470588, alpha: 1).cgColor
        trackLayer.lineWidth = 18
        trackLayer.fillColor = UIColor.clear.cgColor
        //        trackLayer.lineCap = kCALineCapRound
        viTest.layer.addSublayer(trackLayer)
        
        shapLayer.path = circlePath.cgPath
        shapLayer.strokeColor = #colorLiteral(red: 0.1725490196, green: 0.5764705882, blue: 0.2509803922, alpha: 1).cgColor
        shapLayer.lineWidth = 18
        shapLayer.fillColor = UIColor.clear.cgColor
        //        shapLayer.lineCap = kCALineCapRound
        shapLayer.strokeEnd = 0
        //============================
        //============================
        
        
        viTest.layer.addSublayer(shapLayer)
        
        
        
    }
    
    func pauseAnimation(layer : CALayer){
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
    }
    
    func resumeAnimation(layer : CALayer){
        let pausedTime = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
    }
    
    
    func convertSeconds(time : Int) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        
    }
    
    
}


//MARK:- table view configuration
extension IncommingOrderDetailsViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! IncomingOrderCustomCell
        
        
        
        
        return cell
    }
    
    
}







