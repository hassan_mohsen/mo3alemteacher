//
//  IncomingOrdrsViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/19/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class IncomingOrdrsViewController: UIViewController {

    @IBOutlet weak var tblIncomingOrder: UITableView!
    
    private let sectionTitle = ["اخر 24 ساعة"  ,
                                "سابقآ"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tblIncomingOrder.register(UINib.init(nibName: "IncomingOrderCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
    }

    
    
    
    

}


extension IncomingOrdrsViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! IncomingOrderCustomCell
        
        
        return cell
    }
    
    
}

extension IncomingOrdrsViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vi = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 25))
        
        let lbl = UILabel.init(frame: CGRect(x: 0, y: 0, width: vi.frame.width - 16, height: vi.frame.height))
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        lbl.text = sectionTitle[section]
        
        
        return vi
    }
}
