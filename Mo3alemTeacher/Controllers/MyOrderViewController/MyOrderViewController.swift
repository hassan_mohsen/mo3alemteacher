
//
//  MyOrderViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class MyOrderViewController: UIViewController {

    
    @IBOutlet weak var tblMyOrder: UITableView!
    
    var tableData = [[[String : AnyObject]]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMyOrder.register(UINib.init(nibName: "MyOrderCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        
    }

    

}

extension MyOrderViewController : UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyOrderCustomCell
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section].count
    }
}

