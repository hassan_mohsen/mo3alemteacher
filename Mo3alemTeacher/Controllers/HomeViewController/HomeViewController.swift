//
//  HomeViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

let CONST_HEADER_ON  : CGFloat = 128.0
let CONST_HEADER_OFF : CGFloat = 0.0


class HomeViewController: UIViewController {

    //===================================
    
    @IBOutlet weak var imgAvilability: UIImageView!
    @IBOutlet weak var lblAvailability: UILabel!
    @IBOutlet weak var viRingContainer: UIView!
    @IBOutlet weak var viHeaderContainer: UIView!
    @IBOutlet weak var lblRemainTime: UILabel!
    @IBOutlet weak var btnResumPause: UIButton!
    @IBOutlet weak var viRingTimer: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var constContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var viStatus: UIView!
    //===================
    var timer : SwiftCountDownTimer!
    
    let shapLayer = CAShapeLayer()
    
    var On = true
    //=============
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.title = "الرئيسية"
        self.navigationController?.navigationBar.tintColor = .white 
        //=================================
//        constContainerHeight.constant = CONST_HEADER_ON
        constContainerHeight.constant = CONST_HEADER_OFF
//        setupRingLayer(viTest: viRingTimer)
        //=================================
        if let userData = UserDataActions.getUserModel() {
            imgProfile.image = userData.gender == 0 ? #imageLiteral(resourceName: "MALE") : #imageLiteral(resourceName: "FEMALE")
            imgProfile.moa.url = userData.image
//            userData.isAvailable
            lblAvailability.text = userData.isAvailable == 1 ? StatusType.available.rawValue : StatusType.notAvailable.rawValue
            viStatus.backgroundColor = userData.isAvailable == 1 ? #colorLiteral(red: 0.07843137255, green: 0.7490196078, blue: 0.5137254902, alpha: 1) : #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            
        }
        //=================================
        
        
    }

    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func tapGestureTest(_ sender: Any) {
        
        if On {
            
            pauseAnimation(layer: shapLayer)
            timer.suspend()
        }else{
            
            resumeAnimation(layer: shapLayer)
            timer.start()

        }
        On = !On
    }
   
    
    
    override func viewWillAppear(_ animated: Bool) {
        setupTimerAndRingAnimation()
        
        
    }
   
    
    @IBAction func btnPauseAndResumAct(_ sender: Any) {
    }
    
    
    @IBAction func btnNewOrdersAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "IncomingOrdrsViewController") as! IncomingOrdrsViewController
        show(vc, sender: nil)
    }
    @IBAction func btnMyFinancialAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "MyFinancialRecordsViewController") as! MyFinancialRecordsViewController
        show(vc, sender: nil)
        
    }
    @IBAction func btnAvaliabilityAct(_ sender: Any) {
        
        let changeViewModel = ChangeStatusViewModel()
        
        
        
        McPicker.show(data: [[StatusType.available.rawValue , StatusType.notAvailable.rawValue]]) {[weak self] (selection : [Int:String]) in
            
            let selected = selection[0]
            if selected == StatusType.available.rawValue  && selected != self?.lblAvailability.text {
                changeViewModel.setStatusData(status: "1")
            }else if selected == StatusType.notAvailable.rawValue  && selected != self?.lblAvailability.text {
                changeViewModel.setStatusData(status: "0")
            }
            
            changeViewModel.SuccessEventWithMessage = {[weak self](message) in
                
                self?.lblAvailability.text = selected
                self?.viStatus.backgroundColor = selected == StatusType.available.rawValue ? #colorLiteral(red: 0.07843137255, green: 0.7490196078, blue: 0.5137254902, alpha: 1) : #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                
                UserDataActions.changeTeacherStatus(status: selected == StatusType.available.rawValue ? 1 : 0 )
                
            }
            changeViewModel.ErrorEventWithMessage = {[weak self] (message) in
                self?.showErrorWithMessage(message: message)
            }
            
            
        }
    }

    @IBAction func MyVotesAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "MyVoteViewController") as! MyVoteViewController
        self.show(vc, sender: nil)
        
    }
    @IBAction func SettingsButAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "SetttingsViewController") as! SetttingsViewController
        show(vc, sender: nil)
        
    }
    
    
    

}

// MARK :- ring and timer setup
extension HomeViewController
{
    
    func setupTimerAndRingAnimation() -> Void {
        timer = SwiftCountDownTimer(interval: .fromSeconds(1), times: 200, handler: {[unowned self] (timer, lefttimer) in
            self.lblRemainTime.text = "\(self.convertSeconds(time: lefttimer))\n remain"
        })
        timer.start()
        
        
        let basicanimation = CABasicAnimation(keyPath: "strokeEnd")
        basicanimation.toValue = 1
        basicanimation.duration = 200
        basicanimation.fillMode = kCAFillModeForwards
        basicanimation.isRemovedOnCompletion = false
        
        
        shapLayer.add(basicanimation, forKey: "urSoBasic")
    }
    func setupRingLayer(viTest : UIView) -> Void {
        
//        let center = viTest.center
        let center = CGPoint(x: viTest.bounds.width / 2.0, y: viTest.bounds.height / 2.0)
        // creat track layer
        let trackLayer = CAShapeLayer()
        let circlePath = UIBezierPath(arcCenter: center, radius: 48, startAngle: -CGFloat.pi/2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circlePath.cgPath
        trackLayer.strokeColor = #colorLiteral(red: 0.5294117647, green: 0.831372549, blue: 0.7176470588, alpha: 1).cgColor
        trackLayer.lineWidth = 18
        trackLayer.fillColor = UIColor.clear.cgColor
        //        trackLayer.lineCap = kCALineCapRound
        viTest.layer.addSublayer(trackLayer)
        
        shapLayer.path = circlePath.cgPath
        shapLayer.strokeColor = #colorLiteral(red: 0.1725490196, green: 0.5764705882, blue: 0.2509803922, alpha: 1).cgColor
        shapLayer.lineWidth = 18
        shapLayer.fillColor = UIColor.clear.cgColor
        //        shapLayer.lineCap = kCALineCapRound
        shapLayer.strokeEnd = 0
        //============================
        //============================
        
        
        viTest.layer.addSublayer(shapLayer)
        
        
        
    }
    
    func pauseAnimation(layer : CALayer){
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
    }
    
    func resumeAnimation(layer : CALayer){
        let pausedTime = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
    }
    
    
    func convertSeconds(time : Int) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        
    }
    
}
//================================


enum StatusType : String {
    case available = "متاح"
    case notAvailable = "غير متاح"
}
