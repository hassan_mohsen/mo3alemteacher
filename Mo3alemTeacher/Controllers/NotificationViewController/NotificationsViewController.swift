//
//  NotificationsViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    
    @IBOutlet weak var tblNotification: UITableView!
    
    var tableData = [[String : AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblNotification.register(UINib.init(nibName: "NotificationCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        
    }

    
    
    
}


extension NotificationsViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationCustomCell
        
        
        
        return cell
    }
}

extension NotificationsViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "اخر 24 ساعه"
        }else{
            return "سابقآ"
        }
    }
    
    
}


