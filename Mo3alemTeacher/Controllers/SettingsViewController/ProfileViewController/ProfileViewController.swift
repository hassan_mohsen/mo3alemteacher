//
//  ProfileViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    
    @IBOutlet weak var tblProfile: UITableView!
    
    var tableData = [[String : AnyObject]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblProfile.register(UINib.init(nibName: "ProfileCustomCell", bundle: nil), forCellReuseIdentifier: "cell") 
        
        
        
        tblProfile.tableFooterView = footerTableView()
        
        tableData.append(["one" : "two" as AnyObject])
        tblProfile.reloadData()
        
    }

    

}



extension ProfileViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfileCustomCell
        
        
        
        
        
        return cell
    }
    
    
}





//MARK:- helper methods
extension ProfileViewController
{
    func footerTableView() -> UIView {
        let vi = UIView.init(frame: CGRect(x: 0, y: 0, width: tblProfile.bounds.width, height: 70))
        vi.backgroundColor = .clear
        
        let but = UIButton.init(frame: CGRect(x: 25, y: 28, width: tblProfile.bounds.width - 50, height: 35))
        but.backgroundColor = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
        but.setTitle("ملاحظاتك", for: .normal)
        but.setTitleColor(.white, for: .normal)
        but.setImage(UIImage.init(named: "ProfileNote")!, for: .normal)
        but.semanticContentAttribute = .forceRightToLeft
        but.layer.cornerRadius = 5.0
        
        
        but.addTarget(self, action: #selector(myNotesButAct) , for: .touchUpInside)
        
        
        vi.addSubview(but)
        return vi
    }
    
    
    @objc private func myNotesButAct() -> Void {
        
        
        
        
    }
}


