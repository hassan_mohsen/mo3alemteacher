//
//  BanksSliderViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class BanksSliderViewController: UIViewController {

    
    @IBOutlet weak var collectBanks: UICollectionView!
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    var selectedIndexPath : IndexPath!
    var collectData : [BanksModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //====================================
        collectBanks.register(UINib.init(nibName: "BanksSliderCustomCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        pageIndicator.numberOfPages = collectData.count
        print("index path \(selectedIndexPath.item)")
        //====================================
        
    }
    override func viewWillAppear(_ animated: Bool) {
        collectBanks.selectItem(at: selectedIndexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    override func viewDidLayoutSubviews() {
        pageIndicator.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 2, y: 2)
        }
        pageIndicator.currentPage = selectedIndexPath.item
    }
    
    @IBAction func DoneButAct(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closeAct(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    

}


extension BanksSliderViewController : UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BanksSliderCustomCell
        
        let obj = collectData[indexPath.item]
        
//        cell.cellImgHeader.backgroundColor = obj.BackColor
        cell.cellImgHeader.image = obj.Image
        cell.cellViContainer.backgroundColor = obj.BackColor
        
        
        cell.backgroundColor = .white
        
        
        
        return cell
    }
    
    
}




extension BanksSliderViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageIndicator.currentPage = indexPath.item
        pageIndicator.updateCurrentPageDisplay()
        print("page current \(pageIndicator.currentPage) >>>>> \(indexPath.item)")
    }
    
    
    
}





