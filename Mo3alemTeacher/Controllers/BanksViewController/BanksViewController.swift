//
//  BanksViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class BanksViewController: UIViewController {

    
    @IBOutlet weak var collectBanks: UICollectionView!
    
    var banksDataSource = [BanksModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //============================================
        collectBanks.register(UINib.init(nibName: "BanksCustomCellCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        //============================================
        banksDataSource = BanksModel.getDataSource()
        //===========================================
        
        
    }

    
    @IBAction func btnMakeSureOfTrans(_ sender: Any) {
        
    }
    
}

extension BanksViewController : UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return banksDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BanksCustomCellCell
        
        let mod = banksDataSource[indexPath.item]
        
        cell.cellImgBank.image = mod.Image
//        cell.cellImgBank.backgroundColor = mod.BackColor
        cell.backgroundColor = mod.BackColor
        
        
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    
}

extension BanksViewController : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "BanksSliderViewController") as! BanksSliderViewController
        vc.selectedIndexPath = indexPath
        vc.collectData = banksDataSource
        present(vc, animated: true, completion: nil)
        
    }
    
    
}


extension BanksViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
}






