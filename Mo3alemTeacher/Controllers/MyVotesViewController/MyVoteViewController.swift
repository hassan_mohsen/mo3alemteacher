//
//  MyVoteViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/24/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit
import  Cosmos
class MyVoteViewController: UIViewController {
    //==============================
    @IBOutlet weak var lblFiveNumber: UILabel!
    @IBOutlet weak var lblFourthNumber: UILabel!
    @IBOutlet weak var lblThirdNumber: UILabel!
    @IBOutlet weak var lblSecondNumber: UILabel!
    @IBOutlet weak var lblFirstNumber: UILabel!
    //==============================
    @IBOutlet weak var viAveragRate: CosmosView!
    @IBOutlet weak var viFiveVotesProgess: LinearProgressView!
    @IBOutlet weak var viFourthVotesProgess: LinearProgressView!
    @IBOutlet weak var viThirdVotesProgess: LinearProgressView!
    @IBOutlet weak var viSecondVotesProgess: LinearProgressView!
    @IBOutlet weak var viFirstVotesProgess: LinearProgressView!
    //==============================
    @IBOutlet weak var tblMyVotes: UITableView!
    //==============================
    let listRateViewModel = ListRatesViewModel()
    //==============================
    var tableData = [StudentsRate]() {
        didSet {
            tblMyVotes.reloadData()
        }
    }
    //==============================
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblMyVotes.register(UINib.init(nibName: "MyVotesCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        listRateViewModel.SuccessEventWithResult = {[weak self] (model) in
            self?.tableData = model.studentsRates!
            
            if let stars = model.starts {
                
                let keys = Array(stars.keys)
                let keysInt = keys.flatMap({ (val) -> Int in
                    return Int(val)!
                })
                let values = Array(stars.values)
                let sumOfValues = values.reduce(0, +)
                let sumOf2Array = (zip(keysInt , values)).map({$0.0 * $0.1 }).reduce(0, +)// multiplay 2 array and then plus every value
                
                
                self?.viAveragRate.rating = Double(sumOf2Array/sumOfValues)
                self?.viAveragRate.text = "\(Double(sumOf2Array/sumOfValues))"
                
                
                self?.viFiveVotesProgess.maximumValue = Float(sumOfValues)
                self?.viFourthVotesProgess.maximumValue = Float(sumOfValues)
                self?.viThirdVotesProgess.maximumValue = Float(sumOfValues)
                self?.viSecondVotesProgess.maximumValue = Float(sumOfValues)
                self?.viFirstVotesProgess.maximumValue = Float(sumOfValues)
                
                self?.viFiveVotesProgess.setProgress(Float(stars["5"] ?? 0), animated: true)
                self?.viFourthVotesProgess.setProgress(Float(stars["4"] ?? 0) , animated: true)
                self?.viThirdVotesProgess.setProgress(Float(stars["3"] ?? 0) , animated: true)
                self?.viSecondVotesProgess.setProgress(Float(stars["2"] ?? 0) , animated: true)
                self?.viFirstVotesProgess.setProgress(Float(stars["1"] ?? 0) , animated: true)
                
                
            }
            
        }
        
        listRateViewModel.FailureEventWithMessage = {[weak self] (message) in
            self?.showErrorWithMessage(message: message)
        }
        
        listRateViewModel.getVotesData()
        
    }

    
}


extension MyVoteViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MyVotesCustomCell
        
        let obj = tableData[indexPath.row]
        
        cell.cellLblName.text = obj.studentName
        cell.cellViRate.rating = Double(obj.total!)
        
        
        
        return cell
    }
}

extension MyVoteViewController : UITableViewDelegate
{
    //For any Additions in delegations methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
}
