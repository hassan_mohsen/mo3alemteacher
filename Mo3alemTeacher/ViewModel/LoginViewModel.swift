//
//  LoginViewModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

typealias OperationClouser = (() -> Void)
typealias OperationClouserWithStrParameter = ((_ message: String) -> Void)


class ClousersBase {
    var successEvent : OperationClouser?
    var failureEvent : OperationClouserWithStrParameter?
    
}



class LoginViewModel : ClousersBase {
    
//    var successEvent : OperationClouser?
//    var failureEvent : OperationClouserWithStrParameter?
    
    private var e_mail : String!
    private var password : String!
    
    
    
    
    func setModelValues(email : String , pass : String) -> Void {
        self.e_mail = email
        self.password = pass
    }
    
    func makeMeLogin() -> Void {
        let parameter = ["email" : e_mail! ,
                         "password" : password! ,
                         "type" : "1"] as [String : AnyObject]
        
        RestAPIRequest.SendRequest(mod: BaseModel<UserLogin>.self, urlPath: RoutesFactory.URLClear(route: .login).link, method: .post, parameter: parameter) { (response) in
                print("in view model")
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                    // minus cash Data
                    UserDataActions.cashUserModel(user: model.data!)
                    self.successEvent!()
                }else{
                    self.failureEvent!(model.error!.joined(separator: ","))
                }
                
            case .failure(let error):
                self.failureEvent!((error?.localizedDescription)!)
            }
        }
        
    }
    
    
    
    
    
    
    
}
