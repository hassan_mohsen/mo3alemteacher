//
//  ListRatesViewModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 9/10/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

class ListRatesViewModel {
    
    var SuccessEventWithResult : ((_ model : ListRatesModel) -> Void)!
    var FailureEventWithMessage : ((_ message : String)-> Void)!
    
    
    func getVotesData() -> Void {
        RestAPIRequest.SendRequest(mod: BaseModel<ListRatesModel>.self, urlPath: RoutesFactory.URLwithTokenParameter(route: .listRates).link, parameter: nil) { (response) in
            switch response {
                
            case .success(let model):
                if model.status == 200  {
                    self.SuccessEventWithResult!(model.data!)
                }else{
                    self.FailureEventWithMessage!(model.error?.joined(separator: ",") ?? "هناك شئ خاطئ")
                }
            case .failure(let error):
                self.FailureEventWithMessage!((error?.localizedDescription)!)
            }
        }
    }
    
    
}

