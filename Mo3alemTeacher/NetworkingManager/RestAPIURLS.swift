//
//  RestAPIURLS.swift
//  Aoun
//
//  Created by hassan on 5/6/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

let BASE_URL = "http://jadeerapp.com/api/"

public enum APIRoutes : String {
    case login = "login"
    case subject = "subjects"
    case forgetPassword = "forgetPassword"
    case cities = "cities"
    case checkCode = "checkCode"
    case resetPassword = "resetPassword"
    case availableTeacher = "availableTeacher"
    case resendCode = "resendCode"
    case listRates = "listRates"
    
    
    
}



//Warning:- Minus Token Variable



enum RoutesFactory {
    
    case URLwithTokenParameter(route : APIRoutes)
    case URLClear(route:APIRoutes)
    case URLWithOneParameter(route : APIRoutes , key : String , value : String)
    
    var link : URL {
        switch self {
        case .URLwithTokenParameter(let route):
            return URL.init(string: BASE_URL + route.rawValue + "?apiToken=" + UserDataActions.getApiToken())!
        case .URLClear(let route):
            return URL.init(string: BASE_URL + route.rawValue)!
        case .URLWithOneParameter(let route, let key, let value) :
            return URL.init(string: BASE_URL + route.rawValue + "?" + key + "=" + value)!
            
//        default:
//            return URL.init(string: BASE_URL)!
        }
        
    }
}






