//
//  BanksModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import  UIKit
struct BanksModel {
    var Image : UIImage!
    var BackColor : UIColor!
    
    init(image : String , color : UIColor) {
        Image = UIImage.init(named: image)
        BackColor = color
    }
    
    static func getDataSource() -> [BanksModel] {
        return [BanksModel(image: "BankRaghy", color: #colorLiteral(red: 0.1137254902, green: 0.262745098, blue: 0.5803921569, alpha: 1)) ,
                BanksModel(image: "BankAlahly", color: #colorLiteral(red: 0, green: 0.3764705882, blue: 0.3098039216, alpha: 1)) ,
                BanksModel(image: "BankEnmaa", color: #colorLiteral(red: 0.2666666667, green: 0.09411764706, blue: 0.05490196078, alpha: 1)) ,
                BanksModel(image: "BankSaab", color: #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)) ]
    }
    
}
