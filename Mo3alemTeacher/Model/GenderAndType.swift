//
//  GenderAndType.swift
//  mo3alemStudent
//
//  Created by hassan on 6/24/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


enum GenderAndType : String {
    case StudentMale  = "studentMale"
    case StudentFemale = "studentFemale"
    case TeacherMale = "TeacherMale"
    case TeacherFemale = "TeacherFemale"
}

enum GenderType : String  {
    case Male = "MALE"
    case Female = "FEMALE"
}
