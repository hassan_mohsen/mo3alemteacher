//
//  SubjectModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


struct SubjectModel : Codable {
    
    let name : String?
    let minprice : String?
    let maxprice : String?
    let stageid : String?
    let status : String?
    
    
    enum CodingKeys: String, CodingKey {
        case minprice = "min_price"
        case stageid  = "stage_id"
        case maxprice = "max_price"
        case name , status
    }
    
}
