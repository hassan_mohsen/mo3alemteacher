//
//  BaseModelProtocol.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


struct BaseModel<T : Codable> : Codable {
    
    let status: Int?
    let error: [String]?
    let message: String?
    let data:T?
    
    
}
