//
//  CitiesModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


struct CitiesModel : Codable {

    let name : String?
    let parentID : Int?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        case parentID = "parent_id"
        case name , status
    }
    
}



